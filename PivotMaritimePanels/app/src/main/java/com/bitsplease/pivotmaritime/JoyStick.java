package com.bitsplease.pivotmaritime;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

public class JoyStick {
    PointOfViewCameraPanelFragment povf;
    private int STICK_ALPHA = 200;
    private int LAYOUT_ALPHA = 200;
    private int OFFSET = 0;
    private Context mContext;
    private ViewGroup mLayout;
    private LayoutParams params;
    private int stick_width, stick_height;

    private int position_x = 0, position_y = 0, min_distance = 0;
    private float distance = 0, angle = 0;

    //this int represents the y value that gets drawn
    private int drawY = 0;
    private DrawCanvas draw;
    private Paint paint;
    private Bitmap stick;
    private boolean pitch_on = false;
    private boolean touch_state = false;

    public JoyStick(Context context, ViewGroup layout, int stick_res_id) {
        mContext = context;

        stick = BitmapFactory.decodeResource(mContext.getResources(),
                stick_res_id);

        stick_width = stick.getWidth();
        stick_height = stick.getHeight();

        draw = new DrawCanvas(mContext);
        paint = new Paint();
        mLayout = layout;
        params = mLayout.getLayoutParams();
    }

    /**
     * Sets the variable pitch_on to true. When pitch is on joystick only works on Y axis
     */
    public void turnPitchOn(){
        pitch_on = true;
    }

    /**
     *  Sets the variable pitch_on to false. When pitch is off joystick can work 360 degrees
     */
    public void turnPitchOff(){
        pitch_on = false;
    }

    /**
     * Moves the stick of the joystick back to the center.
     * This will occur everytime the user lets go of the joystick
     */
    public void initialiseStickToCenter()
    {
        //initialise stick position
        float x = (float) (params.width / 2);
        float y = (float) (params.height / 2);

        draw.position(x, y);
        drawY = (int)y;
        draw();
    }

    /**
     * This will draw the stick onto the joystick depending on where the users moves it.
     * Uses the draw method to do the actual drawing. This method is figuring out where the user is moving it
     * and making sure the users movement is inside the boundary of the background of the joystick.
     *
     * @param arg1 The users movement of the stick
     */
    public void drawStick(MotionEvent arg1) {
        notifyObserver();
        position_x = (int) (arg1.getX() - (params.width / 2));
        position_y = (int) (arg1.getY() - (params.height / 2));
        distance = (float) Math.sqrt(Math.pow(position_x, 2) + Math.pow(position_y, 2));
        angle = (float) cal_angle(position_x, position_y);


        if(arg1.getAction() == MotionEvent.ACTION_DOWN) {
            if(distance <= (params.width / 2) - OFFSET) {
                draw.position(arg1.getX(), arg1.getY());
                drawY = (int)arg1.getY();
                draw();
                touch_state = true;
            }
        } else if(arg1.getAction() == MotionEvent.ACTION_MOVE && touch_state) {
            if(distance <= (params.width / 2) - OFFSET) {
                draw.position(arg1.getX(), arg1.getY());
                drawY = (int)arg1.getY();
                draw();
            } else if(distance > (params.width / 2) - OFFSET){
                float x = (float) (Math.cos(Math.toRadians(cal_angle(position_x, position_y))) * ((params.width / 2) - OFFSET));
                float y = (float) (Math.sin(Math.toRadians(cal_angle(position_x, position_y))) * ((params.height / 2) - OFFSET));
                x += (params.width / 2);
                y += (params.height / 2);
                draw.position(x, y);
                drawY = (int)y;
                draw();
            } else {
                mLayout.removeView(draw);
            }
        } else if(arg1.getAction() == MotionEvent.ACTION_UP) {
            //mLayout.removeView(draw);
            float x = (float) (params.width / 2);
            float y = (float) (params.height / 2);

            draw.position(x, y);
            drawY = (int)y;
            draw();
            touch_state = false;
        }
    }


    // Not currently in use
    public void setMinimumDistance(int minDistance) {
        min_distance = minDistance;
    }

    /**
     * Used to help set the boundary of where the stick can move to in relation to its background image
     *
     * @param offset an int that will determine how close to boundary stick will move
     */
    public void setOffset(int offset) {
        OFFSET = offset;
    }

    /**
     * Sets the transparency of the stick.
     * The lower the number the more transparent the stick will be
     *
     * @param alpha int that determines the transparency
     */
    public void setStickAlpha(int alpha) {
        STICK_ALPHA = alpha;
        paint.setAlpha(alpha);
    }
    /**
     * Sets the transparency of the layout.
     * The lower the number the more transparent the layout will be
     *
     * @param alpha int that determines the transparency
     */
    public void setLayoutAlpha(int alpha) {
        LAYOUT_ALPHA = alpha;
        mLayout.getBackground().setAlpha(alpha);
    }

    /**
     * Used to set the size of the joysticks stick
     *
     * @param width The width you would like the stick to be
     * @param height The height you would like the stick to be
     */
    public void setStickSize(int width, int height) {
        stick = Bitmap.createScaledBitmap(stick, width, height, false);
        stick_width = stick.getWidth();
        stick_height = stick.getHeight();
    }

    /**
     * Sets the size of the joysticks background image.
     *
     * @param width the width you would like the background to be
     * @param height the height you would like the background to be
     */
    public void setLayoutSize(int width, int height) {
        params.width = width;
        params.height = height;
    }


    /**
     * Calculates the angle of the joystick
     *
     * @param x Position of joystick on X axis
     * @param y Position of joystick on Y axis
     * @return
     */
    private double cal_angle(float x, float y) {
        if(x >= 0 && y >= 0)
            return Math.toDegrees(Math.atan(y / x));
        else if(x < 0 && y >= 0)
            return Math.toDegrees(Math.atan(y / x)) + 180;
        else if(x < 0 && y < 0)
            return Math.toDegrees(Math.atan(y / x)) + 180;
        else if(x >= 0 && y < 0)
            return Math.toDegrees(Math.atan(y / x)) + 360;
        return 0;
    }

    /**
     * This method does the actual drawing of the stick onto the background
     * Must check if the pitch is on. If it is will only draw on the Y axis.
     *
     */
    private void draw() {
        if(pitch_on){
            float x = (float) (params.width / 2);
            draw.position(x, drawY);
        }
        try {
            mLayout.removeView(draw);
        } catch (Exception e) { }
        mLayout.addView(draw);
    }





    /**
     * Method used to register the joystick in the fragment it belongs, the PointOfViewCamera
     * It can then report progress once its registered
     *
     * @param povf Fragment that the joystick will report progress updates to
     */
    public void registerObserver(PointOfViewCameraPanelFragment povf) {
        this.povf = povf;
    }

    /**
     * Notifies the fragment that contains the joystick that a progress change has occurred
     *
     */
    public void notifyObserver() {
        povf.joyStickUpdate();
    }

    /**
     * Used to determine if pitch button is on/off
     *
     * @return
     */
    public boolean isPitchOn() {
        return pitch_on;
    }

    /**
     * Private class to help with the drawing of joystick
     *
     */
    private class DrawCanvas extends View{
        float x, y;


        private DrawCanvas(Context mContext) {
            super(mContext);
        }

        public void onDraw(Canvas canvas) {
            canvas.drawBitmap(stick, x, y, paint);
        }

        private void position(float pos_x, float pos_y) {
            x = pos_x - (stick_width/2);
            y = pos_y - (stick_height/2);
        }


    }
}
