package com.bitsplease.pivotmaritime;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

public class WinchHorizontalSlider {

    private WinchControlPanelFragment hf;
    private Context mContext;
    private ViewGroup mLayout;
    private LayoutParams params;
    private int stick_width, stick_height;
    private int truex, truey;
    private DrawCanvas draw;
    private Paint paint;
    private Bitmap stick;
    private boolean touch_state = false;
    private double progress = 0;
    private double left_boundary, right_boundary;

    /**
     * Constructor
     * @param context
     * @param layout
     * @param stick_res_id
     */
    public WinchHorizontalSlider(Context context, ViewGroup layout, int stick_res_id) {
        mContext = context;
        stick = BitmapFactory.decodeResource(mContext.getResources(), stick_res_id);
        stick_width = stick.getWidth();
        stick_height = stick.getHeight();
        draw = new DrawCanvas(mContext);
        paint = new Paint();
        mLayout = layout;
        params = mLayout.getLayoutParams();
    }

    /**
     * Method used to draw the slider stick onto the slider background.
     * @param arg1 The users touch
     */
    public void drawStick(MotionEvent arg1) {
        if(arg1.getAction() == MotionEvent.ACTION_DOWN) {
            if(arg1.getX()>left_boundary  && arg1.getX()<right_boundary) {

                // progress = -(((arg1.getX()-left_boundary)-0)/(right_boundary-left_boundary-0)*100) ;
                // ^ explanation below
                // (arg1.getX()-left_boundary)-0)/(right_boundary-left_boundary-0) is normalising the value range to 0-1
                // normalising as in (val-min)/(max-min)
                // this is multiplied by 200
                // this gives range of (0 to 200) from top to bottom but we want range of (100 to -100)
                // function f(x)=-x+100 means x = 0 is converted to 100 and x = 200 is converted to -100
                progress = (((arg1.getX()-left_boundary)-0)/(right_boundary-left_boundary-0)*100) ;
                if(!closeToZero()) {
                    draw.position(arg1.getX());
                }
                draw();

                notifyObserver();
                //System.out.println(progress);
                touch_state = true;
            }
            else if(arg1.getX()<left_boundary){
                draw.position((int)left_boundary);
                draw();
                progress = (0/(right_boundary-0)*100) ;
                notifyObserver();
                // System.out.println(progress);
                touch_state = true;
            }
            else if(arg1.getX()>right_boundary){
                draw.position((int)right_boundary);
                draw();
                progress = (((right_boundary)-0)/(right_boundary-0)*100) ;
                notifyObserver();
                // System.out.println(progress);
                touch_state = true;
            }
        } else if(arg1.getAction() == MotionEvent.ACTION_MOVE && touch_state) {
            if(arg1.getX()>left_boundary  && arg1.getX()<right_boundary) {
                progress = (((arg1.getX()-left_boundary)-0)/(right_boundary-left_boundary-0)*100) ;
                if(!closeToZero()) {
                    draw.position(arg1.getX());
                }
                draw();
                notifyObserver();
                //  System.out.println(progress);
            }
            else if(arg1.getX()<left_boundary){
                draw.position((int)left_boundary);
                draw();
                progress = ( 0/(right_boundary-0)*100);
                notifyObserver();
                //    System.out.println(progress);
            }
            else if(arg1.getX()>right_boundary){
                draw.position((int)right_boundary);
                draw();
                progress = (((right_boundary)-0)/(right_boundary-0)*100) ;
                notifyObserver();
                //     System.out.println(progress);
            }
        } else if(arg1.getAction() == MotionEvent.ACTION_UP) {
            draw();
            touch_state = false;
        }
    }

    /**
     * Used to create the size of the slider knob
     * Usually used by the fragment that contains the slider.
     *
     * @param width the width of the knob
     * @param height the height of the knob
     */
    public void setStickSize(int width, int height) {
        stick = Bitmap.createScaledBitmap(stick, width, height, false);
        stick_width = stick.getWidth();
        stick_height = stick.getHeight();
    }

    /**
     * This method draw the knob onto the screen.
     * Also removes where the knob was before the movement.
     */
    private void draw() {
        try {
            mLayout.removeView(draw);
        } catch (Exception e) { }
        mLayout.addView(draw);
    }

    /**
     * Private class to help with the drawing of slider
     *
     */
    private class DrawCanvas extends View{
        float x, y;

        private DrawCanvas(Context mContext) {
            super(mContext);
        }

        public void onDraw(Canvas canvas) {
            canvas.drawBitmap(stick, x, y, paint);
        }

        private void position(float pos_x) {
            x = pos_x - (stick_width / 2);
            y = getTruey()/2 - (stick_height / 2);
        }
    }

    /**
     * x and y are passed in from WinchControlPanelFragment.
     * @param x
     * @param y
     */
    public void passXY(int x, int y) {
        truex = x;
        truey = y;
    }

    /**
     * Centers the slider
     *
     */
    public void center() {
        draw.position(getTruex()/2);
        draw();
        progress = 50;
        notifyObserver();
    }

    /**
     * Determines where progress is near 0 and if it is, then center the slider.
     * @return
     */
    public boolean closeToZero(){
        if(progress<60&&progress>40){
            progress = 50;
            center();
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Sets left boundary
     * @param left
     */
    public void setLeftBoundary(double left){
        left_boundary = getTruex()*left;
    }

    /**
     * Same concept as setTopBoundary in WinchControlSlider class but would deal with inputs closer to 1.0,
     * like 0.9 to capture bottom boundary.
     * @param right
     */
    public void setRightBoundary(double right){
        right_boundary = getTruex()*right;
    }

    public int getTruex(){
        return truex;
    }

    public int getTruey(){
        return truey;
    }


    /**
     * Method used to register the slider in the fragment it belongs, the Winch Control Fragment
     * Now it can report progress changes
     *
     * @param hf Fragment that houses the slider
     */
    public void registerObserver(WinchControlPanelFragment hf) {
        this.hf = hf;
    }

    /**
     * Notifies the fragment that houses the slider that a progress change has occurred
     *
     */
    public void notifyObserver() { hf.winchHorizontalUpdate(progress); }
}