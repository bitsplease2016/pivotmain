package com.bitsplease.pivotmaritime;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class TelegraphSlider {
    private TelegraphFragment tf;
    private String name = "";
    private Context mContext;
    private ViewGroup mLayout;
    private int stick_width, stick_height;
    private int background_width, background_height;
    private double top_boundary, bottom_boundary;
    private double progress;
    private DrawCanvas draw;
    private Paint paint;
    private Bitmap stick;
    private boolean touch_state = false;
    private double touchProgress = 0;


    /**
     * Constructor initialises the variables for the TelegraphSlider.
     * @param context
     * @param layout
     * @param stick_res_id
     */
    public TelegraphSlider(Context context, ViewGroup layout, int stick_res_id) {
        mContext = context;
        stick = BitmapFactory.decodeResource(mContext.getResources(), stick_res_id);
        stick_width = stick.getWidth();
        stick_height = stick.getHeight();
        draw = new DrawCanvas(mContext);
        paint = new Paint();
        mLayout = layout;
        progress = 0.0;
    }

    /**
     * This method takes in the current progress value neededProgress and compares it to the latest
     * touch event (actualProgress). If the finger touches close enough to the current progress
     * then the method returns true, allowing the user to move the slider and update the progress as a result of that
     *
     * @param neededProgress
     * @param actualProgress
     * @return
     */
    public boolean closeEnough(double neededProgress, double actualProgress){
        if(Math.abs(neededProgress-actualProgress)<20){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Method used to draw the slider stick onto the slider.
     * Uses the closeEnough method to determine whether the users touch is on the current position of the slider knob.
     * If it is then it will use the users touch (arg1) to determine where the user is moving and draw on the new slider knob
     * @param arg1 The users touch
     */
    public void drawStick(MotionEvent arg1) {
        //touchProgress calculates what progress your finger it at when it touches down
        touchProgress = -(((arg1.getY()-bottom_boundary)-0)/(top_boundary-bottom_boundary-0)*-200) - 100;


        //if touchProgress is between 0-20 away from previous progress value then draw slider knob and update the progress
        if(closeEnough(progress,touchProgress)) {

            if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
                if (arg1.getY() > top_boundary && arg1.getY() < bottom_boundary) {
                    progress = -(((arg1.getY() - top_boundary) - 0) / (bottom_boundary - top_boundary - 0) * 200) + 100;

                    draw.position(arg1.getY());

                    draw();
                    // progress = -(((arg1.getY()-top_boundary)-0)/(bottom_boundary-top_boundary-0)*200) + 100;
                    // ^ explanation below
                    // (arg1.getY()-top_boundary)-0)/(bottom_boundary-top_boundary-0) is normalising the value range to 0-1
                    // normalising as in (val-min)/(max-min)
                    // this is multiplied by 200
                    // this gives range of (0 to 200) from top to bottom but we want range of (100 to -100)
                    // function f(x)=-x+100 means x = 0 is converted to 100 and x = 200 is converted to -100

                    notifyObserver();
                    //System.out.println(progress);
                    touch_state = true;
                } else if (arg1.getY() < top_boundary) {
                    draw.position((int) top_boundary);
                    draw();
                    progress = -(0 / (bottom_boundary - 0) * 200) + 100;
                    notifyObserver();
                    // System.out.println(progress);
                    touch_state = true;
                } else if (arg1.getY() > bottom_boundary) {
                    draw.position((int) bottom_boundary);
                    draw();
                    progress = -(((bottom_boundary) - 0) / (bottom_boundary - 0) * 200) + 100;
                    notifyObserver();
                    // System.out.println(progress);
                    touch_state = true;
                }
            } else if (arg1.getAction() == MotionEvent.ACTION_MOVE && touch_state) {
                if (arg1.getY() > top_boundary && arg1.getY() < bottom_boundary) {
                    progress = -(((arg1.getY() - top_boundary) - 0) / (bottom_boundary - top_boundary - 0) * 200) + 100;

                    draw.position(arg1.getY());

                    draw();
                    notifyObserver();
                    //  System.out.println(progress);
                } else if (arg1.getY() < top_boundary) {
                    draw.position((int) top_boundary);
                    draw();
                    progress = -(0 / (bottom_boundary - 0) * 200) + 100;
                    notifyObserver();
                    //    System.out.println(progress);
                } else if (arg1.getY() > bottom_boundary) {
                    draw.position((int) bottom_boundary);
                    draw();
                    progress = -(((bottom_boundary) - 0) / (bottom_boundary - 0) * 200) + 100;
                    notifyObserver();
                    //     System.out.println(progress);
                }
            } else if (arg1.getAction() == MotionEvent.ACTION_UP) {
                draw();
                touch_state = false;
            }
        }
    }

    /**
     * This method draw the knob onto the screen.
     * Also removes where the knob was before the movement.
     */
    private void draw() {
        try {
            mLayout.removeView(draw);
        } catch (Exception e) { }
        mLayout.addView(draw);
    }

    /**
     * Private class to help with the drawing of slider
     *
     */
    private class DrawCanvas extends View{
        float x, y;

        private DrawCanvas(Context mContext) {
            super(mContext);
        }

        public void onDraw(Canvas canvas) {
            canvas.drawBitmap(stick, x, y, paint);
        }

        /**
         * This method sets the position for the slider knob to be rendered on.
         * The x-axis stays constant but the y-axis changes allowing only vertical movement.
         * @param pos_y is the touch event y-axis value
         */
        private void position(float pos_y) {
            x = getBackgroundWidth()/2 - (stick_width / 2);
            y = pos_y - (stick_height / 2);
        }
    }

    /**
     * Sets the size of the slider knob
     * @param width
     * @param height
     */
    public void setStickSize(int width, int height) {
        stick = Bitmap.createScaledBitmap(stick, width, height, false);
        stick_width = stick.getWidth();
        stick_height = stick.getHeight();
    }

    /**
     * Returns background_width
     * @return background_width
     */
    public int getBackgroundWidth(){
        return background_width;
    }

    /**
     * Sets the background width variable of this class from Telegraph Fragment
     * @param background_width
     */
    public void setBackgroundWidth(int background_width) {
        this.background_width = background_width;
    }

    /**
     * Sets the background height variable of this class from Telegraph Fragment
     * @param background_height
     */
    public void setBackgroundHeight(int background_height) {
        this.background_height = background_height;
    }


    /**
     * Given a background you may want to set boundaries for where the stick can be drawn.
     * the y axis of background is counted starting from the top, so at top it is 0 and at bottom
     * it is the value of the background height.
     * if setTopBoundary is set to 0.1 it means a boundary at the top is set at 10% of background height
     * so if background height is 600, then the stick can't be drawn between 0 and 60 on y axis.
     *
     * @param top
     */
    public void setTopBoundary(double top){
        top_boundary = background_height*top;
    }



    /**
     * Same concept as setTopBoundary but would deal with inputs closer to 1.0, like 0.9 to capture bottom boundary
     *
     * @param bottom
     */
    public void setBottomBoundary(double bottom){
        bottom_boundary = background_height*bottom;
    }

    /**
     * Centers the slider
     *
     */
    public void center(){
        draw.position(background_height/2);
        draw();
        progress = 0;
        notifyObserver();
    }


    /**
     * Method used to register the slider in the fragment it belongs, the Telegraph Fragment
     * Now it can report progress changes
     * @param tf
     */
    public void registerObserver(TelegraphFragment tf) {
        this.tf = tf;
    }

    /**
     * Notifies the fragment that houses the slider that a progress change has occurred
     *
     */
    public void notifyObserver() { tf.telegraphSliderUpdate(progress, this); }

    /**
     * Returns name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets TelegraphSlider's name from param
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

}