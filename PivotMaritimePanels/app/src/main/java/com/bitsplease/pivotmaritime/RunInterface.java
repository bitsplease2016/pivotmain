package com.bitsplease.pivotmaritime;

/**
 * Created by stewartburnell on 24/08/16.
 */

/**
 * This interface exists so that you can use an object of it as a parameter for a method where you want to implement
 * a unique method body for that particular case.
 *
 * An specific example of its use is with the ViewTreeObserverUtility class. The ViewTreeObserverUtility class has a static
 * method called SetUpViewTreeObserver that takes as parameters; a View object and a RunInterface object.
 * Implementing the RunInterface's run method gives a body of code to be executed as soon as the View has finished
 * inflating.
 *
 * This was made to solve the issue of calling a method of eg. a Button that had not finished inflating.
 * This in turn would cause a NullPointerException because the Button object returned from findViewById returned
 * a null object.
 */

public interface RunInterface {
        void run();
}

