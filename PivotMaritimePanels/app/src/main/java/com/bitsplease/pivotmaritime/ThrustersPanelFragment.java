package com.bitsplease.pivotmaritime;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ToggleButton;



public class ThrustersPanelFragment extends MainPanelFragment {
    //private final String name = "THRUSTERS";
    //private State state;
    //private MainActivity ma;

    private Handler handler;
    //Starts off false then is set to true after all the button super.getState()s are read in
    private boolean initialised = false;
    private boolean motorsStopIsOn = false;
    private boolean stopState = false;

    //Arrays of the Thrusters ToggleButtons
    ToggleButton[] thrusters_bow_buttons = new ToggleButton[5];
    ToggleButton[] thrusters_stern_buttons = new ToggleButton[5];
    ToggleButton[] all_buttons = new ToggleButton[12];
    
    int sternActiveButton;
    int bowActiveButton;

    private ToggleButton thrusters_bow_red_full;
    private ToggleButton thrusters_bow_red_half;
    private ToggleButton thrusters_bow_yellow_stop;
    private ToggleButton thrusters_bow_green_half;
    private ToggleButton thrusters_bow_green_full;
    private ToggleButton thrusters_motors_red_stop;
    private ToggleButton thrusters_motors_green_start;
    private ToggleButton thrusters_stern_red_full;
    private ToggleButton thrusters_stern_red_half;
    private ToggleButton thrusters_stern_yellow_stop;
    private ToggleButton thrusters_stern_green_half;
    private ToggleButton thrusters_stern_green_full;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_panel_thrusters, container, false);
    }

    /**
     * Initialises Button Variables, Button Arrays and also calls
     * methods to set buttons to previous state or new state.
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ViewTreeObserverUtility.SetUpViewTreeObserver(getView(), new RunInterface() {
            @Override
            public void run() {
                thrusters_bow_red_full = (ToggleButton) getView().findViewById(R.id.thrusters_bow_red_full);
                thrusters_bow_red_half = (ToggleButton) getView().findViewById(R.id.thrusters_bow_red_half);
                thrusters_bow_yellow_stop = (ToggleButton) getView().findViewById(R.id.thrusters_bow_yellow_stop);
                thrusters_bow_green_half = (ToggleButton) getView().findViewById(R.id.thrusters_bow_green_half);
                thrusters_bow_green_full = (ToggleButton) getView().findViewById(R.id.thrusters_bow_green_full);
                thrusters_motors_red_stop = (ToggleButton) getView().findViewById(R.id.thrusters_motors_red_stop);
                thrusters_motors_green_start = (ToggleButton) getView().findViewById(R.id.thrusters_motors_green_start);
                thrusters_stern_red_full = (ToggleButton) getView().findViewById(R.id.thrusters_stern_red_full);
                thrusters_stern_red_half = (ToggleButton) getView().findViewById(R.id.thrusters_stern_red_half);
                thrusters_stern_yellow_stop = (ToggleButton) getView().findViewById(R.id.thrusters_stern_yellow_stop);
                thrusters_stern_green_half = (ToggleButton) getView().findViewById(R.id.thrusters_stern_green_half);
                thrusters_stern_green_full = (ToggleButton) getView().findViewById(R.id.thrusters_stern_green_full);

                thrusters_bow_buttons[0] = thrusters_bow_red_full;
                thrusters_bow_buttons[1] = thrusters_bow_red_half;
                thrusters_bow_buttons[2] = thrusters_bow_yellow_stop;
                thrusters_bow_buttons[3] = thrusters_bow_green_half;
                thrusters_bow_buttons[4] = thrusters_bow_green_full;

                thrusters_stern_buttons[0] = thrusters_stern_red_full;
                thrusters_stern_buttons[1] = thrusters_stern_red_half;
                thrusters_stern_buttons[2] = thrusters_stern_yellow_stop;
                thrusters_stern_buttons[3] = thrusters_stern_green_half;
                thrusters_stern_buttons[4] = thrusters_stern_green_full;

                all_buttons[0] = thrusters_bow_red_full;
                all_buttons[1] = thrusters_bow_red_half;
                all_buttons[2] = thrusters_bow_yellow_stop;
                all_buttons[3] = thrusters_bow_green_half;
                all_buttons[4] = thrusters_bow_green_full;
                all_buttons[5] = thrusters_motors_red_stop;
                all_buttons[6] = thrusters_motors_green_start;
                all_buttons[7] = thrusters_stern_red_full;
                all_buttons[8] = thrusters_stern_red_half;
                all_buttons[9] = thrusters_stern_yellow_stop;
                all_buttons[10] = thrusters_stern_green_half;
                all_buttons[11] = thrusters_stern_green_full;

                loadState();



                if(initialised==false){
                    initialise();
                }
                else{
                    cantClickBowButton(returnActiveBowButton());
                    cantClickSternButton(returnActiveSternButton());
                    if(thrusters_motors_green_start.isChecked()){
                        thrusters_motors_green_start.setClickable(false);
                    }
                }

                if(motorsStopIsOn){
                    disableButtons();
                }

                //If you pressed stop and left the panel before it cycled to the stop super.getState() then you came back
                if(stopState){
                    turnOffButtons(thrusters_bow_buttons);
                    turnOffButtons(thrusters_stern_buttons);
                    thrusters_bow_yellow_stop.setChecked(true);
                    thrusters_stern_yellow_stop.setChecked(true);
                    initialise();
                    stopState = false;
                }
            }
        });
    }

    /**
     * Initialises all the thruster Bow and Stern Buttons to the toggle
     * off state
     * <p>
     * This method is called when the fragment is created and also
     * during the stop routine.
     */

    public void initialise(){
        disableButtons();
        initialised = true;
        motorsStopIsOn = true;
    }

    /**
     * This method takes what button on the thruster panel has been
     * pressed. Then calls the appropriate button press method.
     *
     * @param  buttonPressed  ToggleButton object of the pressed button
     */
    public void genericButtonPress(ToggleButton buttonPressed) {
        if(buttonPressed==thrusters_bow_red_full){
            pressBowRedFull();
        }
        else if(buttonPressed==thrusters_bow_red_half){
            pressBowRedHalf();
        }
        else if(buttonPressed==thrusters_bow_yellow_stop){
            pressBowYellowStop();
        }
        else if(buttonPressed==thrusters_bow_green_half){
            pressBowGreenHalf();
        }
        else if(buttonPressed==thrusters_bow_green_full){
            pressBowGreenFull();
        }
        else if(buttonPressed==thrusters_motors_red_stop){
            pressMotorsRedStop();
        }
        else if(buttonPressed==thrusters_motors_green_start){
            pressMotorsGreenStart();
        }
        else if(buttonPressed==thrusters_stern_red_full){
            pressSternRedFull();
        }
        else if(buttonPressed==thrusters_stern_red_half){
            pressSternRedHalf();
        }
        else if(buttonPressed==thrusters_stern_yellow_stop){
            pressSternYellowStop();
        }
        else if(buttonPressed==thrusters_stern_green_half){
            pressSternGreenHalf();
        }
        else if(buttonPressed==thrusters_stern_green_full){
            pressSternGreenFull();
        }
        saveState();

    }

    /**
     * Calls appropriate methods and sets state of thruster panel
     * buttons once Bow Red Full button is pressed. Panel state
     * is determined by pivot maritimes requirements.
     * <p>
     * The method toggles all bow buttons off, then toggles the bow
     * button that was pressed, to the on state. {@link State#sendMessage(String) sendMessage}
     * is called using the Bow Red Full argument, in order to
     * save the state. The button pressed is made unclickable, to
     * avoid clicking the same button multiple times.
     */

    public void pressBowRedFull(){
        turnOffButtons(thrusters_bow_buttons);
        thrusters_bow_red_full.setChecked(true);
        super.getState().sendMessage("thrusters_bow_red_full");
        cantClickBowButton(0);

    }

    /**
     * Calls appropriate methods and sets state of thruster panel
     * buttons once Bow Red Half button is pressed. Panel state
     * is determined by pivot maritimes requirements.
     * <p>
     * The method toggles all bow buttons off, then toggles the bow
     * button that was pressed, to the on state. {@link State#sendMessage(String) sendMessage}
     * is called using the Bow Red Half argument, in order to
     * save the state. The button pressed is made unclickable, to
     * avoid clicking the same button multiple times.
     */

    public void pressBowRedHalf(){
        turnOffButtons(thrusters_bow_buttons);
        thrusters_bow_red_half.setChecked(true);
        super.getState().sendMessage("thrusters_bow_red_half");
        cantClickBowButton(1);

    }

    /**
     * Calls appropriate methods and sets state of thruster panel
     * buttons once Bow Yellow Stop button is pressed. Panel state
     * is determined by pivot maritimes requirements.
     * <p>
     * The method toggles all bow buttons off, then toggles the bow
     * button that was pressed, to the on state. {@link State#sendMessage(String) sendMessage}
     * is called using the Bow Yellow Stop argument, in order to
     * save the state. The button pressed is made unclickable, to
     * avoid clicking the same button multiple times.
     */

    public void pressBowYellowStop(){
        turnOffButtons(thrusters_bow_buttons);
        thrusters_bow_yellow_stop.setChecked(true);
        super.getState().sendMessage("thrusters_bow_yellow_stop");
        cantClickBowButton(2);

    }

    /**
     * Calls appropriate methods and sets state of thruster panel
     * buttons once Bow Green Half button is pressed. Panel state
     * is determined by pivot maritimes requirements.
     * <p>
     * The method toggles all bow buttons off, then toggles the bow
     * button that was pressed, to the on state. {@link State#sendMessage(String) sendMessage}
     * is called using the Bow Green Half argument, in order to
     * save the state. The button pressed is made unclickable, to
     * avoid clicking the same button multiple times.
     */

    public void pressBowGreenHalf(){
        turnOffButtons(thrusters_bow_buttons);
        thrusters_bow_green_half.setChecked(true);
        super.getState().sendMessage("thrusters_bow_green_half");
        cantClickBowButton(3);

    }

    /**
     * Calls appropriate methods and sets state of thruster panel
     * buttons once Bow Green Full button is pressed. Panel state
     * is determined by pivot maritimes requirements.
     * <p>
     * The method toggles all bow buttons off, then toggles the bow
     * button that was pressed, to the on state. {@link State#sendMessage(String) sendMessage}
     * is called using the Bow Green Full argument, in order to
     * save the state. The button pressed is made unclickable, to
     * avoid clicking the same button multiple times.
     */

    public void pressBowGreenFull(){
        turnOffButtons(thrusters_bow_buttons);
        thrusters_bow_green_full.setChecked(true);
        super.getState().sendMessage("thrusters_bow_green_full");
        cantClickBowButton(4);
    }

    /**
     * Calls appropriate methods and sets state of thruster panel
     * buttons once Motors Red Stop button is pressed. Panel state
     * is determined by pivot maritimes requirements.
     * <p>
     * The method toggles the Motors Green Start button off and sets its
     * state to clickable. Motors Red Stop button is also made unclickable.
     * {@link State#sendMessage(String) sendMessage} is called using the
     * Motors Red Stop argument, in order to save the state. Finally
     * the {@link #stopRoutine() stopRoutine} method is called to begin the
     * deactivation sequence.
     */

    public void pressMotorsRedStop(){
        thrusters_motors_green_start.setChecked(false);
        thrusters_motors_green_start.setClickable(true);
        thrusters_motors_red_stop.setClickable(false);
        motorsStopIsOn=true;
        super.getState().sendMessage("thrusters_motors_red_stop");
        stopRoutine();

    }

    /**
     * Calls appropriate methods and sets state of thruster panel
     * buttons once Motors Green Start button is pressed. Panel state
     * is determined by pivot maritimes requirements.
     * <p>
     * The method toggles the Motors Red Stop button off and sets its
     * state to clickable. Motors Green Start button is also made unclickable.
     * {@link #enableButtons() enableButtons} is called to set appropriate
     * buttons to a clickable state. {@link #cantClickBowButton(int) cantClickBowButton}
     * and {@link #cantClickSternButton(int) cantClickSternButton} are called
     * for the active bow and stern button respectivley. {@link State#sendMessage(String) sendMessage}
     * is called using the Motors Green Start argument, in order to save the state.
     */

    public void pressMotorsGreenStart(){
        thrusters_motors_red_stop.setChecked(false);
        thrusters_motors_red_stop.setClickable(true);
        thrusters_motors_green_start.setClickable(false);
        motorsStopIsOn=false;
        enableButtons();
        cantClickBowButton(returnActiveBowButton());
        cantClickSternButton(returnActiveSternButton());
        super.getState().sendMessage("thrusters_motors_green_start");
    }

    /**
     * Calls appropriate methods and sets state of thruster panel
     * buttons once Stern Red Full button is pressed. Panel state
     * is determined by pivot maritimes requirements.
     * <p>
     * The method toggles all bow buttons off, then toggles the bow
     * button that was pressed, to the on state. {@link State#sendMessage(String) sendMessage}
     * is called using the Stern Red Full argument, in order to
     * save the state. The button pressed is made unclickable, to
     * avoid clicking the same button multiple times.
     */

    public void pressSternRedFull(){
        turnOffButtons(thrusters_stern_buttons);
        thrusters_stern_red_full.setChecked(true);
        super.getState().sendMessage("thrusters_stern_red_full");
        cantClickSternButton(0);
    }

    /**
     * Calls appropriate methods and sets state of thruster panel
     * buttons once Stern Red Half button is pressed. Panel state
     * is determined by pivot maritimes requirements.
     * <p>
     * The method toggles all bow buttons off, then toggles the bow
     * button that was pressed, to the on state. {@link State#sendMessage(String) sendMessage}
     * is called using the Stern Red Half argument, in order to
     * save the state. The button pressed is made unclickable, to
     * avoid clicking the same button multiple times.
     */

    public void pressSternRedHalf(){
        turnOffButtons(thrusters_stern_buttons);
        thrusters_stern_red_half.setChecked(true);
        super.getState().sendMessage("thrusters_stern_red_half");
        cantClickSternButton(1);
    }

    /**
     * Calls appropriate methods and sets state of thruster panel
     * buttons once Stern Yellow Stop button is pressed. Panel state
     * is determined by pivot maritimes requirements.
     * <p>
     * The method toggles all bow buttons off, then toggles the bow
     * button that was pressed, to the on state. {@link State#sendMessage(String) sendMessage}
     * is called using the Stern Yellow Stop argument, in order to
     * save the state. The button pressed is made unclickable, to
     * avoid clicking the same button multiple times.
     */

    public void pressSternYellowStop(){
        turnOffButtons(thrusters_stern_buttons);
        thrusters_stern_yellow_stop.setChecked(true);
        super.getState().sendMessage("thrusters_stern_yellow_stop");
        cantClickSternButton(2);
    }

    /**
     * Calls appropriate methods and sets state of thruster panel
     * buttons once Stern Green Half button is pressed. Panel state
     * is determined by pivot maritimes requirements.
     * <p>
     * The method toggles all bow buttons off, then toggles the bow
     * button that was pressed, to the on state. {@link State#sendMessage(String) sendMessage}
     * is called using the Stern Green Half argument, in order to
     * save the state. The button pressed is made unclickable, to
     * avoid clicking the same button multiple times.
     */

    public void pressSternGreenHalf(){
        turnOffButtons(thrusters_stern_buttons);
        thrusters_stern_green_half.setChecked(true);
        super.getState().sendMessage("thrusters_stern_green_half");
        cantClickSternButton(3);
    }

    /**
     * Calls appropriate methods and sets state of thruster panel
     * buttons once Stern Green Full button is pressed. Panel state
     * is determined by pivot maritimes requirements.
     * <p>
     * The method toggles all bow buttons off, then toggles the bow
     * button that was pressed, to the on state. {@link State#sendMessage(String) sendMessage}
     * is called using the Stern Green Full argument, in order to
     * save the state. The button pressed is made unclickable, to
     * avoid clicking the same button multiple times.
     */

    public void pressSternGreenFull(){
        turnOffButtons(thrusters_stern_buttons);
        thrusters_stern_green_full.setChecked(true);
        super.getState().sendMessage("thrusters_stern_green_full");
        cantClickSternButton(4);
    }


    //need to make turn ons and offs for stern and bow

    /**
     * Takes array of toggle buttons {@param buttons}
     * and toggles all buttons in array to off position.
     *
     * @param buttons
     */
    public void turnOffButtons(ToggleButton[] buttons){
       for(ToggleButton tb: buttons){
           tb.setChecked(false);
       }
    }

    /**
     * Takes array of toggle buttons {@param buttons}
     * and toggles all buttons in array to on position.
     *
     * @param buttons
     */
    public void turnOnButtons(ToggleButton[] buttons){
        for(ToggleButton tb: buttons){
            tb.setChecked(true);
        }
    }

    /**
     * Loads Thruster panel button state from the state class.
     * <p>
     * CANNOT FIND super.getState() in state class?
     */
    public void saveState(){
        super.getState().readThrusterButtonStates(all_buttons);
    }

    /**
     * Loads Thruster panel button state from the state class.
     * <p>
     * CANNOT FIND super.getState() in state class?
     */
    public void loadState(){
        all_buttons = super.getState().sendThrusterButtonStates(all_buttons);
    }

    /**
     * Creates new thread to handle thruster panel stop state power down.
     * <p>
     * {@link #disableAllButtons() disableAllButtons} called first to stop
     * user interaction whilst powering down. A handler is created,
     * calls {@link #moveToStopState() moveToStopState}, to power down
     * motors one increment. This handler is run inside a thread that
     * sleeps for 1 second at a time, slowly powering down the thruster
     * motors. Once all buttons are powered down, they're enabled again
     * via, {@link #enableAllButtons() enableAllButtons}, and the panel
     * state is reinitialised with {@link #initialise() initialise}.
     */
    public void stopRoutine(){
        disableAllButtons();

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                moveToStopState();
            }
        };

        Thread t = new Thread(){
            @Override
            public void run() {
                while(isInStopState() == false){
                    Message message = Message.obtain();
                    try {
                        Thread.sleep(1000);
                        handler.sendMessage(message);
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                    }
                }
                enableAllButtons();
                initialise();
            }
        };
        t.start();

    }

    /**
     * Moves active bow and stern buttons 1 increment towards stop
     * state.
     * <p>
     * {@link #returnActiveBowButton() returnActiveBowButton} and
     * {@link #returnActiveSternButton() returnActiveSternButton}
     * give current motor state. First the bow button state is
     * moved 1 increment closer to the stop state, then the stern
     * is moved.
     */
    public void moveToStopState(){
        stopState = true;
        int bow = returnActiveBowButton();
        int stern = returnActiveSternButton();

        //If there was an active button, ie active button didn't return -1
        if(!(bow==-1)) {
            if (bow < 2) {
                //if active bow button index is less than 2, turn off all
                //bow buttons and turn on the button to right of it
                turnOffButtons(thrusters_bow_buttons);
                thrusters_bow_buttons[bow + 1].setChecked(true);
            } else if (bow > 2) {
                turnOffButtons(thrusters_bow_buttons);
                thrusters_bow_buttons[bow - 1].setChecked(true);
            }
        }
        //Same for stern
        if(!(stern==-1)) {
            if (stern < 2) {
                turnOffButtons(thrusters_stern_buttons);
                thrusters_stern_buttons[stern + 1].setChecked(true);
            } else if (stern > 2) {
                turnOffButtons(thrusters_stern_buttons);
                thrusters_stern_buttons[stern - 1].setChecked(true);
            }
        }

    }


    /**
     * Returns the index of the active bow button.
     * Returns -1 if no active button.
     *
     * @return int for the index of the active bow button
     */
    public int returnActiveBowButton(){
        for (int i = 0; i < thrusters_bow_buttons.length; i++) {
            if(thrusters_bow_buttons[i].isChecked()){
                return i;
            }
        }
        System.out.println("Error. No active buttons.");
        return -1;
    }


    /**
     * Returns the index of the active stern button.
     * Returns -1 if no active button.
     *
     * @return int for the index of the active stern button
     */
    public int returnActiveSternButton(){
        for (int i = 0; i < thrusters_stern_buttons.length; i++) {
            if(thrusters_stern_buttons[i].isChecked()){
                return i;
            }
        }
        System.out.println("Error. No active buttons.");
        return -1;
    }

    /**
     * Enables all Thruster panel buttons, except Motors
     * Green Start.
     * <p>
     * Enables all bow buttons first, followed by all
     * stern buttons. Finally enables Motors Red Stop.
     */
    public void enableButtons(){
        for(ToggleButton tb: thrusters_bow_buttons) {
            tb.setClickable(true);
        }

        for(ToggleButton tb: thrusters_stern_buttons) {
            tb.setClickable(true);
        }

        thrusters_motors_red_stop.setClickable(true);
    }

    /**
     * Enables all Thruster panel buttons.
     * <p>
     * Calls {@link #enableButtons() enableButtons},
     * then additionally enables Motors Green Start button
     * making all buttons on thruster panel enabled.
     */
    public void enableAllButtons(){
        enableButtons();
        thrusters_motors_green_start.setClickable(true);
    }

    /**
     * Disables all Thruster panel buttons, except Motors
     * Green Start.
     * <p>
     * Disables all bow buttons first, followed by all
     * stern buttons. Finally Disables Motors Red Stop.
     */
    public void disableButtons(){
        for(ToggleButton tb: thrusters_bow_buttons) {
            tb.setClickable(false);
        }

        for(ToggleButton tb: thrusters_stern_buttons) {
            tb.setClickable(false);
        }

        thrusters_motors_red_stop.setClickable(false);
    }

    /**
     * Disables all Thruster panel buttons.
     * <p>
     * Calls {@link #disableButtons() disableButtons},
     * then additionally disables Motors Green Start button
     * making all buttons on thruster panel disabled.
     */
    public void disableAllButtons(){
        disableButtons();
        thrusters_motors_green_start.setClickable(false);
    }

    /**
     * Returns true if Thruster panel buttons are in stop state.
     *
     * @return boolean indicating if Thrusters are in stop state
     */
    public boolean isInStopState(){
        if(thrusters_bow_yellow_stop.isChecked()
                && thrusters_motors_red_stop.isChecked()
                && thrusters_stern_yellow_stop.isChecked() ){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        saveState();
    }

    //Get State class object from main activity.  Main activity will make frag object call this and pass super.getState().

    /**
     * sets button located at buttonArrayIndex to unclickable
     * and sets all other bow buttons to clickable.
     *
     * @param buttonArrayIndex
     */
    public void cantClickBowButton(int buttonArrayIndex){
        for (int i = 0; i < thrusters_bow_buttons.length; i++) {
             if(i==buttonArrayIndex){
                 thrusters_bow_buttons[i].setClickable(false);
             }
            else {
                 thrusters_bow_buttons[i].setClickable(true);
             }
        }
    }

    /**
     * sets button located at buttonArrayIndex to unclickable
     * and sets all other stern buttons to clickable.
     *
     * @param buttonArrayIndex
     */
    public void cantClickSternButton(int buttonArrayIndex){
        for (int i = 0; i < thrusters_stern_buttons.length; i++) {
            if(i==buttonArrayIndex){
                thrusters_stern_buttons[i].setClickable(false);
            }
            else {
                thrusters_stern_buttons[i].setClickable(true);
            }
        }
    }

    
}
