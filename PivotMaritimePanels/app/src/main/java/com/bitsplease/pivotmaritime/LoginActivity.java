package com.bitsplease.pivotmaritime;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class LoginActivity extends Activity{
    //  Full screen functionality
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHideRunnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            mContentView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LOW_PROFILE
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            );
        }
    };
    private final Runnable mShowRunnable = new Runnable() {
        @Override
        public void run() {
            mContentView.setVisibility(View.GONE);
        }
    };

    private Button enterAdminActivity;
    private Button returnToApp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //  These must be first two methods called
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        //  Full screen functionality
        mContentView = findViewById(R.id.loginmain);
        hide();
    }

    /**
     * This method is called whenever the fragment comes back into users view.
     * Also called when first being created. Creates a button that the user can click to
     * go back to the app. Also creates a button that will take the user to the admin activity
     * to change which panels the user can see.
     */
    @Override
    protected void onResume() {
        super.onResume();
        //  Hide the system UI.
        hide();



        enterAdminActivity = (Button)findViewById(R.id.enterAdminPanelBtn);
        enterAdminActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String username = ((EditText) findViewById(R.id.nameText)).getText().toString();
                String password = ((EditText) findViewById(R.id.passwordText)).getText().toString();
                if (verifyUsername(username) && verifyPassword(password))
                {
                    Intent i = new Intent(getApplicationContext(), AdminActivity.class);
                    startActivity(i);
                }


            }
        });

        returnToApp = (Button) findViewById(R.id.returnToSimulatorBtn);
        returnToApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
            }
        });
    }

    /**
     * This method is currently just a stub.
     * Can be implemented to determine if the username is correct
     *
     * @param username The username of the admin
     * @return         Whether the username matches
     */
    private boolean verifyUsername(String username)
    {
       // System.out.println( "Username checked: " + username );
        return true;
    }
    /**
     * This method is currently just a stub.
     * Can be implemented to determine if the password is correct
     *
     * @param password The password of the admin
     * @return         Whether the password matches
     */
    private boolean verifyPassword(String password)
    {  // System.out.println( "Password checked: " + password );
        return true;
    }

    /**
     * Hides system UI
     *
     */
    private void hide() {
        mHideHandler.removeCallbacks(mShowRunnable);
        mHideHandler.postDelayed(mHideRunnable, 0);
    }
}
